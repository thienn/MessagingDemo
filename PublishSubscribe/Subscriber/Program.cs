﻿using System;
using System.Text;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Subscriber
{
    public class Program
    {
        private const string RabbitServer = "192.168.137.8";
        private const string ExchangeName = "logs";

        public static void Main(string[] args)
        {
            var factory = new ConnectionFactory { HostName = RabbitServer };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: ExchangeName, type: "fanout");
                var queueName = channel.QueueDeclare().QueueName;
                channel.QueueBind(queue: queueName, exchange: ExchangeName, routingKey: "");

                Console.WriteLine("[*] Waiting for logs.");

                var subscriber = new EventingBasicConsumer(channel);
                subscriber.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine("[x] {0}", message);
                };

                channel.BasicConsume(queue: queueName, noAck: true, consumer: subscriber);
                Console.Read();
            }
        }
    }
}
