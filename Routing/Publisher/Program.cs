﻿using System;
using System.Text;

using RabbitMQ.Client;

namespace Publisher
{
    public class Program
    {
        private const string RabbitServer = "192.168.137.8";
        private const string ExchangeName = "direct_logs";

        public static void Main(string[] args)
        {
            var factory = new ConnectionFactory { HostName = RabbitServer };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: ExchangeName, type: "direct");

                Console.WriteLine("Type some things to sent. severity:message ");

                while (true)
                {
                    var content = Console.ReadLine();
                    var messageParts = content.Split(':');
                    var severity = messageParts[0];
                    var message = messageParts[1];
                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: ExchangeName,
                                         routingKey: severity,
                                         basicProperties: null,
                                         body: body);
                    Console.WriteLine(" [x] Sent '{0}':'{1}'", severity, message);
                }
            }
        }
    }
}
