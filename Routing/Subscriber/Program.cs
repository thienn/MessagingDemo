﻿using System;
using System.Text;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Subscriber
{
    public class Program
    {
        private const string RabbitServer = "192.168.137.8";
        private const string ExchangeName = "direct_logs";

        public static void Main(string[] args)
        {
            if (!ValidateInput(args))
            {
                return;
            }

            var factory = new ConnectionFactory { HostName = RabbitServer };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: ExchangeName, type: "direct");
                var queueName = channel.QueueDeclare().QueueName;

                foreach (var severity in args)
                {
                    channel.QueueBind(queue: queueName, exchange: ExchangeName, routingKey: severity);
                }

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += ConsumerReceived;
                channel.BasicConsume(queue: queueName, noAck: true, consumer: consumer);

                Console.WriteLine(" [*] Waiting for: {0}", string.Join(", ", args));
                Console.ReadLine();
            }
        }

        private static void ConsumerReceived(object sender, BasicDeliverEventArgs e)
        {
            var body = e.Body;
            var message = Encoding.UTF8.GetString(body);
            var routingKey = e.RoutingKey;
            Console.WriteLine(" [x] Received '{0}':'{1}'", routingKey, message);
        }

        private static bool ValidateInput(string[] args)
        {
            if (args.Length < 1)
            {
                Console.Error.WriteLine("Usage: {0} [info] [warning] [error]", Environment.GetCommandLineArgs()[0]);
                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
                Environment.ExitCode = 1;
                return false;
            }

            return true;
        }
    }
}
