﻿using System;
using System.Text;

using RabbitMQ.Client;

namespace Producer
{
    public class Program
    {
        private const string RabbitServer = "192.168.137.8";
        private const string QueueName = "task_queue";
        private const string RoutingKey = "task_queue";

        public static void Main(string[] args)
        {
            var factory = new ConnectionFactory { HostName = RabbitServer };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(
                    queue: QueueName,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                Console.WriteLine("Type something to send...");
                while (true)
                {
                    var message = Console.ReadLine();
                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(
                        exchange: "",
                        routingKey: RoutingKey,
                        basicProperties: null,
                        body: body);
                }
            }
        }
    }
}
