﻿using System;
using System.Text;

using RabbitMQ.Client;

namespace Publisher
{
    public class Program
    {
        private const string RabbitServer = "192.168.137.8";
        private const string ExchangeName = "logs";

        public static void Main(string[] args)
        {
            var factory = new ConnectionFactory { HostName = RabbitServer };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: ExchangeName, type: "fanout");

                Console.WriteLine("Enter something to send");
                while (true)
                {
                    var message = Console.ReadLine();
                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(
                        exchange: ExchangeName,
                        routingKey: "",
                        basicProperties: null,
                        body: body);
                }
            }
        }
    }
}
